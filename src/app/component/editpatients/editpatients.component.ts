import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApipatientsService } from 'src/app/services/apipatients.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editpatients',
  templateUrl: './editpatients.component.html',
  styleUrls: ['./editpatients.component.css']
})
export class EditpatientsComponent implements OnInit {

editform : any
id = this.activeroute.snapshot.params.id
submitted = false

  constructor(private service : ApipatientsService,private route:Router,private activeroute : ActivatedRoute,
    private formbuilder : FormBuilder) { }

  ngOnInit(): void {
    this.editform = this.formbuilder.group({
      name:['',Validators.required],
      email:['',Validators.required],
      role:['',Validators.required],
      naissance:['',Validators.required],
      sex:['',Validators.required],
      telephone:['',Validators.required],
    })

    this.getPatientById()
  }

  get f(){ return this.editform.controls}

  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.editform.invalid) {
        return;
    }
    this.service.SavePatients(this.editform.value).subscribe(res=>{
      Swal.fire(
        'added!',
        'Your Medicament has been added.',
        'success'
      )   
      
    })
  }

  getPatientById(){
    this.service.GetOnePatients(this.id).subscribe(res=>{ 

       this.editform.setValue({
         name:res["patients"].name, 
         email:res["patients"].email,
         role:res["patients"].role,
         naissance:res["patients"].naissance,
         sex:res["patients"].sex,
         telephone:res["patients"].telephone
        
       })

    })
  }

  UpdatePatient(){
    this.submitted=true 
    if(this.editform.invalid) {return ;} 
    this.service.UpdatePatients(this.id,this.editform.value).subscribe(res=>{
     Swal.fire(
       'update!',
       'Your patient has been update.',
       'success'
     )         
          this.route.navigate(['/home/patients'])
   
    })
  }

  onReset(){
    this.editform.reset()
  }

  

}
