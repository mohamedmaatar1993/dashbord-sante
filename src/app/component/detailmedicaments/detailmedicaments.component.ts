import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApimedicamentService } from 'src/app/services/apimedicament.service';

@Component({
  selector: 'app-detailmedicaments',
  templateUrl: './detailmedicaments.component.html',
  styleUrls: ['./detailmedicaments.component.css']
})
export class DetailmedicamentsComponent implements OnInit {

  DetailMedicament :any
  id=this.routeacive.snapshot.params.id

  constructor(private service : ApimedicamentService,private routeacive : ActivatedRoute) { }

  ngOnInit(): void {
  this.GetmedicamentById()

  }

  GetmedicamentById(){
    this.service.GetOneMedicament(this.id).subscribe(res=>{
      this.DetailMedicament=res["medicaments"]   
    })
  }
}
