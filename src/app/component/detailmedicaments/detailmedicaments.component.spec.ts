import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailmedicamentsComponent } from './detailmedicaments.component';

describe('DetailmedicamentsComponent', () => {
  let component: DetailmedicamentsComponent;
  let fixture: ComponentFixture<DetailmedicamentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailmedicamentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailmedicamentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
