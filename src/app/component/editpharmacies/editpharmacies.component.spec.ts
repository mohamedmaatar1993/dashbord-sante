import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditpharmaciesComponent } from './editpharmacies.component';

describe('EditpharmaciesComponent', () => {
  let component: EditpharmaciesComponent;
  let fixture: ComponentFixture<EditpharmaciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditpharmaciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditpharmaciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
