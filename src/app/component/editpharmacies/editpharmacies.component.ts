import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApipharmacieService } from 'src/app/services/apipharmacie.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editpharmacies',
  templateUrl: './editpharmacies.component.html',
  styleUrls: ['./editpharmacies.component.css']
})
export class EditpharmaciesComponent implements OnInit {

  // EditPharmacie : {nom:"",localisation :"",adresse:"",type:"",telephone:""}
  formEdit : FormGroup;
  id=this.activeroute.snapshot.params.id
  submitted=false

  constructor(private service : ApipharmacieService,private activeroute :ActivatedRoute,private formbuilder:FormBuilder,private route:Router) { }

  ngOnInit(): void {

    this.getPharmacieById()

    this.formEdit= this.formbuilder.group({
      nom:['',Validators.required],
      localisation:['',Validators.required],
      adresse:['',Validators.required],
      type:['',Validators.required],
      telephone:['',Validators.required],

    })
  }
  get f(){ return this.formEdit.controls}

  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.formEdit.invalid) {
        return;
    }
    this.service.SavePharmacie(this.formEdit.value).subscribe(res=>{
      Swal.fire(
        'added!',
        'Your Pharmacie has been added.',
        'success'
      )   
    this.getPharmacieById()
    })
  }

  UpdatePharmacie(){
    this.submitted=true 
    if(this.formEdit.invalid) {return ;} 
    this.service.UpdatePharmacies(this.id,this.formEdit.value).subscribe(res=>{
     Swal.fire(
       'update!',
       'Your pharmacie has been update.',
       'success'
     )         
          this.route.navigate(['/home/pharmacies'])
   
    })
  }

  getPharmacieById(){
    this.service.GetOnePharmacies(this.id).subscribe(res=>{ 

       this.formEdit.setValue({
         nom:res["pharmacies"].nom, 
         localisation:res["pharmacies"].localisation,
         adresse:res["pharmacies"].adresse,
         type:res["pharmacies"].type,
         telephone:res["pharmacies"].telephone


        
       })

    })
  }

  onReset(){
    this.formEdit.reset()
  }
}
