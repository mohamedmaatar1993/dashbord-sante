import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApimedicamentService } from 'src/app/services/apimedicament.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-medicaments',
  templateUrl: './medicaments.component.html',
  styleUrls: ['./medicaments.component.css']
})
export class MedicamentsComponent implements OnInit {

  listMedicament = []

  constructor(private service : ApimedicamentService) { }

  ngOnInit(): void {
    this.GetMedicament()
  }

  GetMedicament(){
    
    this.service.GetAllMedicament().subscribe(res=>{
      this.listMedicament = res["medicaments"]
      console.log(res["medicaments"])
    })
  }

  DeleteMedicament(id){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.DeleteMedicament(id).subscribe(res=>{

          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          ) 
          this.GetMedicament()

        })
      }
    })
  }
}
