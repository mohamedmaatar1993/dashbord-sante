import { Component, OnInit } from '@angular/core';
import { ApipatientsService } from 'src/app/services/apipatients.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {

  ListPatient : []

  constructor(private service : ApipatientsService) { }

  ngOnInit(): void {
    this.GetPatient()
  }

  GetPatient(){
    this.service.GetAllPatients().subscribe(res=>{
      this.ListPatient = res ["patients"]
    })
  }

  DeletePatient(id){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.DeletePatients(id).subscribe(res=>{

          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          ) 
          this.GetPatient()

        })
      }
    })
  }
}
