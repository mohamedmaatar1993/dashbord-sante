import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiuserService } from 'src/app/services/apiuser.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  resetForm :FormGroup
  submitted : boolean
  resetLink=this.routeactive.snapshot.params.resetLink

  constructor(private service:ApiuserService,private formbuilder:FormBuilder,private routeactive:ActivatedRoute) { }

  ngOnInit(): void {
    this.resetForm = this.formbuilder.group({
      newPass : ['',Validators.required],
      confirmpassword: ['',Validators.required]

    },
    {validator: this.MustMatch('newPass', 'confirmpassword')
  })

  }

   MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}

reset(){
  this.submitted=true
  // alert('ffffffffffffffffffff')
  //  if (this.resetForm.invalid) {return;}
  const data={
    newPass:this.resetForm.value.newPass,
    resetLink:this.resetLink
  }
  console.log(data)
  this.service.resetpassword(data).subscribe(res=>{
    console.log('hhhhhjhhhjjh',res)
    Swal.fire(
      {
     title: 'password correct!',
     text: 'password changed.',
     icon: 'success'
      })
  })
}

}
