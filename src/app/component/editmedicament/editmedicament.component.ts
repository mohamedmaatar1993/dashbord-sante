import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApimedicamentService } from 'src/app/services/apimedicament.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editmedicament',
  templateUrl: './editmedicament.component.html',
  styleUrls: ['./editmedicament.component.css']
})
export class EditmedicamentComponent implements OnInit {

  EditMedicament : {nom:"",forme :"",prix:"",description:"",ingredient:""}
  formEdit : FormGroup;
  id=this.activeroute.snapshot.params.id
  submitted=false

  constructor(private service : ApimedicamentService, private route:Router
    ,private activeroute :ActivatedRoute,private formbuilder:FormBuilder) { }

  ngOnInit(): void {

    this.getMedicamentById()

    this.formEdit= this.formbuilder.group({
      nom:['',Validators.required],
      forme:['',Validators.required],
      prix:['',Validators.required],
      description:['',Validators.required],
      ingredient:['',Validators.required],

    })
  }

  get f(){ return this.formEdit.controls}

  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.formEdit.invalid) {
        return;
    }
    this.service.SaveMedicament(this.formEdit.value).subscribe(res=>{
      Swal.fire(
        'added!',
        'Your Medicament has been added.',
        'success'
      )   
      this.getMedicamentById()
    })
  }

  UpdateMedicaments(){
    this.submitted=true 
    if(this.formEdit.invalid) {return ;} 
    this.service.UpdateMedicament(this.id,this.formEdit.value).subscribe(res=>{
     Swal.fire(
       'update!',
       'Your medicament has been update.',
       'success'
     )         
          this.route.navigate(['/home/medicaments'])
   
    })
  }

  getMedicamentById(){
    this.service.GetOneMedicament(this.id).subscribe(res=>{ 

       this.formEdit.setValue({
         nom:res["medicaments"].nom, 
         prix:res["medicaments"].prix,
         forme:res["medicaments"].forme,
         description:res["medicaments"].description,
         ingredient:res["medicaments"].ingredient


        
       })

    })
  }

  onReset(){
    this.formEdit.reset()
  }
}
