import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiuserService } from 'src/app/services/apiuser.service';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm : FormGroup
  submitted :boolean

  constructor(private UserS : ApiuserService, private formbuilder : FormBuilder,private route : Router) { }

  ngOnInit(): void {
    this.registerForm = this.formbuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      role: ['', Validators.required],
  })
  }
get f(){ return this.registerForm.controls} 

register(){
   
  this.submitted=true
  if(this.registerForm.invalid) {return ;}   
   console.log(this.registerForm.value)

  this.UserS.register(this.registerForm.value).subscribe(res=>{
    console.log('resultat',res)
    
    if(res['message']==='User Created')
    {
       Swal.fire(
        {
       title: 'Registration Success!',
       text: 'Welcome :)',
       icon: 'success'
        }
      )  
      this.route.navigate(['/login'])
    }
   else 
   {
    Swal.fire(
      {
     title: 'Registration Failed!',
     text: 'Verifier vos validation :)',
     icon: 'error'
      }
    )  
   }
  }, err=>{
    Swal.fire(
      {
     title: 'Registration Failed!',
     text: 'Verifier vos validation :)',
     icon: 'error'
      }
    )  
  })
}




}
