import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApipharmacieService } from 'src/app/services/apipharmacie.service';

@Component({
  selector: 'app-detailspharmacies',
  templateUrl: './detailspharmacies.component.html',
  styleUrls: ['./detailspharmacies.component.css']
})
export class DetailspharmaciesComponent implements OnInit {

  DetailPharmacie :any
  id=this.routeacive.snapshot.params.id
  constructor(private service : ApipharmacieService,private routeacive : ActivatedRoute) { }

  ngOnInit(): void {
   this.GetPharmacieById()
  }

  GetPharmacieById(){
    this.service.GetOnePharmacies(this.id).subscribe(res=>{
      this.DetailPharmacie=res["pharmacies"]   

    })
  }

}
