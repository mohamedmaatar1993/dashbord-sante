import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailspharmaciesComponent } from './detailspharmacies.component';

describe('DetailspharmaciesComponent', () => {
  let component: DetailspharmaciesComponent;
  let fixture: ComponentFixture<DetailspharmaciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailspharmaciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailspharmaciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
