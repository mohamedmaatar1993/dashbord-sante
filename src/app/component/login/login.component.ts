import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiuserService } from 'src/app/services/apiuser.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  loginForm: FormGroup;
  submitted:boolean=false

  constructor(private service:ApiuserService,private route:Router, private formbuilder:FormBuilder) { }

  ngOnInit(): void {
    this.loginForm = this.formbuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
  });
  }
  get f(){ return this.loginForm.controls} 

  login(){
   
    this.submitted=true
    if(this.loginForm.invalid) {return ;}   
    
    this.service.Login(this.loginForm.value).subscribe(res=>{

      if(res['message']=='Invalid email/password!!!') {
        Swal.fire(
          {
         title: 'Authentification Failed!',
         text: 'You are failed.',
         icon: 'error'
          }
        )  
      }
      
      if(res['message']=='user found!!!')
      Swal.fire(
        {
       title: 'Authentification Success!',
       text: 'You are logged with succ.',
       icon: 'success'
        })

        localStorage.setItem("token",res['data'].token)
        localStorage.setItem("refreshtoken",res['data'].refreshtoken)

        localStorage.setItem("user",JSON.stringify(res['data'].user))
        localStorage.setItem("state","0")


        this.route.navigate(['/home'])


})

}
}
