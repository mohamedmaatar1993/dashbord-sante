import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiuserService } from 'src/app/services/apiuser.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {

  ForgetForm : FormGroup
  Submitted : boolean

  constructor(private service:ApiuserService,private formbuilder:FormBuilder) { }

  ngOnInit(): void {
    this.ForgetForm = this.formbuilder.group({
      email : ['',Validators.required]
    })
  }

  get f(){return this.ForgetForm.controls}

  Forget(){
    // alert('fffff')
    this.Submitted=true
    // if (this.forgetForm.invalid) {return;}
    this.service.forgetpassword(this.ForgetForm.value).subscribe(res=>{
      console.log(res)
      Swal.fire(
        {
       title: 'email correct!',
       text: 'You are logged with succ.',
       icon: 'success'
        })

    },
    
    err=>{
      console.log(err)
      Swal.fire(
        {
       title: 'email incorrect!',
       text: 'Verifier vos validation :)',
       icon: 'error'
        }
      ) 
    }
    )


  }
}
