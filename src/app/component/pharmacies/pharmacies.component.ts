import { Component, OnInit } from '@angular/core';
import { ApipharmacieService } from 'src/app/services/apipharmacie.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pharmacies',
  templateUrl: './pharmacies.component.html',
  styleUrls: ['./pharmacies.component.css']
})
export class PharmaciesComponent implements OnInit {

  ListPharmacie : []

  constructor(private serive : ApipharmacieService) { }

  ngOnInit(): void {
    this.GetPharmacie()
  }

  GetPharmacie(){
    this.serive.GetAllPharmacies().subscribe(res=>{
      this.ListPharmacie = res ["pharmacies"]
    
    })
  }

  DeletePharmacie(id){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.serive.DeletePharmacaies(id).subscribe(res=>{

          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          ) 
          this.GetPharmacie()

        })
      }
    })
  }
}
