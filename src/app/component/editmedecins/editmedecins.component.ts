import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApimedecinsService } from 'src/app/services/apimedecins.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editmedecins',
  templateUrl: './editmedecins.component.html',
  styleUrls: ['./editmedecins.component.css']
})
export class EditmedecinsComponent implements OnInit {

  EditForm : FormGroup
  id = this.activeroute.snapshot.params.id
  submitted = false

  constructor(private service : ApimedecinsService,private route : Router,private activeroute :ActivatedRoute,private formbuilder:FormBuilder) { }

  ngOnInit(): void {
    this.getMedecinById()
    this.EditForm = this.formbuilder.group({
      name:['',Validators.required],
      email:['',Validators.required],
      role:['',Validators.required],
      specialite:['',Validators.required],
      diplome:['',Validators.required],
      localisation:['',Validators.required],
      tel_mobile:['',Validators.required],
      tel_fix:['',Validators.required],
      adresse:['',Validators.required],



    })
  }

  get f(){ return this.EditForm.controls}

  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.EditForm.invalid) {
        return;
    }
    this.service.SaveMedecins(this.EditForm.value).subscribe(res=>{
      Swal.fire(
        'added!',
        'Your Medicament has been added.',
        'success'
      )   
      
    })
  }

  getMedecinById(){
    this.service.GetOneMedecins(this.id).subscribe(res=>{ 

       this.EditForm.setValue({
         name:res["medecins"].name, 
         email:res["medecins"].email,
         role:res["medecins"].role,
         specialite:res["medecins"].specialite,
         diplome:res["medecins"].diplome,
         localisation:res["medecins"].localisation,
         tel_mobile:res["medecins"].tel_mobile,
         tel_fix:res["medecins"].tel_fix,
         adresse:res["medecins"].adresse,
        
       })

    })
  }

  UpdateMedecin(){
    this.submitted=true 
    if(this.EditForm.invalid) {return ;} 
    this.service.UpdateMedecins(this.id,this.EditForm.value).subscribe(res=>{
     Swal.fire(
       'update!',
       'Your medecin has been update.',
       'success'
     )         
          this.route.navigate(['/home/medecins'])
   
    })
  }

  onReset(){
    this.EditForm.reset()
  }
}
