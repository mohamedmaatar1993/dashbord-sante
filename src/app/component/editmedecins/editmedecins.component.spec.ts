import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditmedecinsComponent } from './editmedecins.component';

describe('EditmedecinsComponent', () => {
  let component: EditmedecinsComponent;
  let fixture: ComponentFixture<EditmedecinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditmedecinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditmedecinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
