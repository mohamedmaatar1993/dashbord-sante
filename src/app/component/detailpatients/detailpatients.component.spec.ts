import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailpatientsComponent } from './detailpatients.component';

describe('DetailpatientsComponent', () => {
  let component: DetailpatientsComponent;
  let fixture: ComponentFixture<DetailpatientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailpatientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailpatientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
