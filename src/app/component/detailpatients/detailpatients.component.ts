import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApipatientsService } from 'src/app/services/apipatients.service';

@Component({
  selector: 'app-detailpatients',
  templateUrl: './detailpatients.component.html',
  styleUrls: ['./detailpatients.component.css']
})
export class DetailpatientsComponent implements OnInit {

DetailPatient : any
id= this.activeroute.snapshot.params.id

  constructor(private service : ApipatientsService, private activeroute : ActivatedRoute) { }

  ngOnInit(): void {
    this.GetPatientById()
  }

  GetPatientById(){
    this.service.GetOnePatients(this.id).subscribe(res=>{
    this.DetailPatient= res ["patients"]
    })
  }

}
