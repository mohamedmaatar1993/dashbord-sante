import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApimedecinsService } from 'src/app/services/apimedecins.service';

@Component({
  selector: 'app-detailmedecins',
  templateUrl: './detailmedecins.component.html',
  styleUrls: ['./detailmedecins.component.css']
})
export class DetailmedecinsComponent implements OnInit {

  DetailMedecin : any
  id=this.activeroute.snapshot.params.id

  constructor(private service : ApimedecinsService, private activeroute:ActivatedRoute) { }

  ngOnInit(): void {
    this.GetMedecinById()
  }

  GetMedecinById(){
    this.service.GetOneMedecins(this.id).subscribe(res=>{
      this.DetailMedecin = res["medecins"]
    })
    
  }

}
