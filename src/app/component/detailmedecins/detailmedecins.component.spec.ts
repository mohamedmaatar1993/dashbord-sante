import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailmedecinsComponent } from './detailmedecins.component';

describe('DetailmedecinsComponent', () => {
  let component: DetailmedecinsComponent;
  let fixture: ComponentFixture<DetailmedecinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailmedecinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailmedecinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
