import { Component, OnInit } from '@angular/core';
import { ApimedecinsService } from 'src/app/services/apimedecins.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-medecins',
  templateUrl: './medecins.component.html',
  styleUrls: ['./medecins.component.css']
})
export class MedecinsComponent implements OnInit {

  ListMedecin : []

  constructor(private service :ApimedecinsService) { }

  ngOnInit(): void {
    this.GetMedecins()

  }

  GetMedecins(){
    this.service.GetAllMedecins().subscribe(res=>{
      this.ListMedecin = res["medecins"]
      console.log(res["medecins"])
    })
  }

  DeleteMedecin(id){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.DeleteMedecins(id).subscribe(res=>{

          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          ) 
          this.GetMedecins()

        })
      }
    })
  }
}
