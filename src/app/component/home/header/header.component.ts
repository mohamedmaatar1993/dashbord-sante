import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiuserService } from 'src/app/services/apiuser.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private service : ApiuserService,private route:Router) { }

  ngOnInit(): void {
  }
  logout(){
    // alert('dfdfdf')
    this.service.logout().subscribe(res=>{
      localStorage.clear()
      this.route.navigate(["/"])
    })
  }

}
