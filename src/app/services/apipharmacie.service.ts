import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApipharmacieService {

  constructor(private http : HttpClient) { }

  SavePharmacie(newPharmacie){
    return this.http.post(`${environment.baseUrl}/pharmacies/save`,newPharmacie)
  }

  GetAllPharmacies(){
    return this.http.get(`${environment.baseUrl}/pharmacies/getall`)
  }

  GetOnePharmacies(idPharmacie){
    return this.http.get(`${environment.baseUrl}/pharmacies/getone/${idPharmacie}`)
  }

  DeletePharmacaies(idPharmacie){
    return this.http.delete(`${environment.baseUrl}/pharmacies/delete/${idPharmacie}`)
  }

  UpdatePharmacies(idPharmacie,newPharmacie){
    return this.http.put(`${environment.baseUrl}/pharmacies/update/${idPharmacie}`,newPharmacie)
  }
}
