import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApimedicamentService {

  constructor(private http : HttpClient) { }

  SaveMedicament(newMedicament){
    return this.http.post(`${environment.baseUrl}/medicaments/save`,newMedicament)
  }

  GetAllMedicament(){
    return this.http.get(`${environment.baseUrl}/medicaments/getall`)
  }

  GetOneMedicament(idMedicament){
    return this.http.get(`${environment.baseUrl}/medicaments/getone/${idMedicament}`)
  }

  DeleteMedicament(idMedicament){
    return this.http.delete(`${environment.baseUrl}/medicaments/delete/${idMedicament}`)
  }

  UpdateMedicament(idMedicament,newMedicament){
    return this.http.put(`${environment.baseUrl}/medicaments/update/${idMedicament}`,newMedicament)
  }
}
