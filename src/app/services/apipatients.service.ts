import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApipatientsService {

  constructor(private http : HttpClient) { }

  SavePatients(newPatient){
    return this.http.post(`${environment.baseUrl}/patients/save`,newPatient)
  }

  GetAllPatients(){
    return this.http.get(`${environment.baseUrl}/patients/getall`)
  }

  GetOnePatients(idpatient){
    return this.http.get(`${environment.baseUrl}/patients/getone/${idpatient}`)
  }

  DeletePatients(idpatient){
    return this.http.delete(`${environment.baseUrl}/patients/delete/${idpatient}`)
  }

  UpdatePatients(idpatient,newPatient){
    return this.http.put(`${environment.baseUrl}/patients/update/${idpatient}`,newPatient)
  }
}
