import { TestBed } from '@angular/core/testing';

import { ApipharmacieService } from './apipharmacie.service';

describe('ApipharmacieService', () => {
  let service: ApipharmacieService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApipharmacieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
