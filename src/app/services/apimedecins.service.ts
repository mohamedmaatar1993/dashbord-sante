import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApimedecinsService {

  constructor(private http : HttpClient) { }

  SaveMedecins(newMedecin){
    return this.http.post(`${environment.baseUrl}/medecins/save`,newMedecin)
  }

  GetAllMedecins(){
    return this.http.get(`${environment.baseUrl}/medecins/getall`)
  }

  GetOneMedecins(idMedecin){
    return this.http.get(`${environment.baseUrl}/medecins/getone/${idMedecin}`)
  }

  DeleteMedecins(idMedecin){
    return this.http.delete(`${environment.baseUrl}/medecins/delete/${idMedecin}`)
  }

  UpdateMedecins(idMedecin,newMedecin){
    return this.http.put(`${environment.baseUrl}/medecins/update/${idMedecin}`,newMedecin)
  }
}
