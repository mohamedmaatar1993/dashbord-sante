import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiuserService {
  token=localStorage.getItem('token')

  constructor(private http : HttpClient) { }

  headers=new HttpHeaders({
    'x-access-token':this.token
  })

  register(user:any){
    return this.http.post(`${environment.baseUrl}/users/save`,user)
  }

  Login(user:any){
    return this.http.post(`${environment.baseUrl}/users/login`,user)
  }

  forgetpassword(user:any){
    return this.http.post(`${environment.baseUrl}/users/forgetpassword`,user)
  }

  resetpassword (user:any){
    return this.http.post(`${environment.baseUrl}/users/resetpassword`,user,{headers:this.headers})
  }

  logout(){
    const refreshToken=localStorage.getItem('refreshToken')
    return this.http.post(`${environment.baseUrl}/users/logout`,{refreshToken:refreshToken})
  }
}
