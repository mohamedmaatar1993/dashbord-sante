import { TestBed } from '@angular/core/testing';

import { ApipatientsService } from './apipatients.service';

describe('ApipatientsService', () => {
  let service: ApipatientsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApipatientsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
