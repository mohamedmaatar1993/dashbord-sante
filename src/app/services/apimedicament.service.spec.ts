import { TestBed } from '@angular/core/testing';

import { ApimedicamentService } from './apimedicament.service';

describe('ApimedicamentService', () => {
  let service: ApimedicamentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApimedicamentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
