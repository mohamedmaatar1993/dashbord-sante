import { TestBed } from '@angular/core/testing';

import { ApimedecinsService } from './apimedecins.service';

describe('ApimedecinsService', () => {
  let service: ApimedecinsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApimedecinsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
