import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { HeaderComponent } from './component/home/header/header.component';
import { FooterComponent } from './component/home/footer/footer.component';
import { LayoutComponent } from './component/home/layout/layout.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './component/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './component/login/login.component';
import { ForgetpasswordComponent } from './component/forgetpassword/forgetpassword.component';
import { ResetpasswordComponent } from './component/resetpassword/resetpassword.component';
import { MedicamentsComponent } from './component/medicaments/medicaments.component';
import { DetailmedicamentsComponent } from './component/detailmedicaments/detailmedicaments.component';
import { EditmedicamentComponent } from './component/editmedicament/editmedicament.component';
import { PharmaciesComponent } from './component/pharmacies/pharmacies.component';
import { DetailspharmaciesComponent } from './component/detailspharmacies/detailspharmacies.component';
import { EditpharmaciesComponent } from './component/editpharmacies/editpharmacies.component';
import { MedecinsComponent } from './component/medecins/medecins.component';
import { DetailmedecinsComponent } from './component/detailmedecins/detailmedecins.component';
import { EditmedecinsComponent } from './component/editmedecins/editmedecins.component';
import { PatientsComponent } from './component/patients/patients.component';
import { EditpatientsComponent } from './component/editpatients/editpatients.component';
import { DetailpatientsComponent } from './component/detailpatients/detailpatients.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LayoutComponent,
    SidebarComponent,
    RegisterComponent,
    LoginComponent,
    ForgetpasswordComponent,
    ResetpasswordComponent,
    MedicamentsComponent,
    DetailmedicamentsComponent,
    EditmedicamentComponent,
    PharmaciesComponent,
    DetailspharmaciesComponent,
    EditpharmaciesComponent,
    MedecinsComponent,
    DetailmedecinsComponent,
    EditmedecinsComponent,
    PatientsComponent,
    EditpatientsComponent,
    DetailpatientsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
