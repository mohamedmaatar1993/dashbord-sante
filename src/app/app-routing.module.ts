import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailmedecinsComponent } from './component/detailmedecins/detailmedecins.component';
import { DetailmedicamentsComponent } from './component/detailmedicaments/detailmedicaments.component';
import { DetailpatientsComponent } from './component/detailpatients/detailpatients.component';
import { DetailspharmaciesComponent } from './component/detailspharmacies/detailspharmacies.component';
import { EditmedecinsComponent } from './component/editmedecins/editmedecins.component';
import { EditmedicamentComponent } from './component/editmedicament/editmedicament.component';
import { EditpatientsComponent } from './component/editpatients/editpatients.component';
import { EditpharmaciesComponent } from './component/editpharmacies/editpharmacies.component';
import { ForgetpasswordComponent } from './component/forgetpassword/forgetpassword.component';
import { HomeComponent } from './component/home/home.component';
import { LayoutComponent } from './component/home/layout/layout.component';
import { LoginComponent } from './component/login/login.component';
import { MedecinsComponent } from './component/medecins/medecins.component';
import { MedicamentsComponent } from './component/medicaments/medicaments.component';
import { PatientsComponent } from './component/patients/patients.component';
import { PharmaciesComponent } from './component/pharmacies/pharmacies.component';
import { RegisterComponent } from './component/register/register.component';
import { ResetpasswordComponent } from './component/resetpassword/resetpassword.component';
import { GuardsGuard } from './guards.guard';


const routes: Routes = [

  {path :'home',component:HomeComponent,canActivate:[GuardsGuard],children:[
    {path:'',component:LayoutComponent},
  {path:'medicaments',component:MedicamentsComponent},
  {path:'detailmedicament/:id',component:DetailmedicamentsComponent},
  {path:'editmedicament/:id',component:EditmedicamentComponent},
  {path:'pharmacies',component:PharmaciesComponent},
  {path:'detailpharmacie/:id',component:DetailspharmaciesComponent},
  {path:'editpharmacie/:id',component:EditpharmaciesComponent},
  {path:'medecins',component:MedecinsComponent},
  {path:'detailmedecin/:id',component:DetailmedecinsComponent},
  {path:'editmedecin/:id',component:EditmedecinsComponent},
  {path:'patients',component:PatientsComponent},
  {path:'detailpatient/:id',component:DetailpatientsComponent},
  {path:'editpatient/:id',component:EditpatientsComponent},




  ]},
  {path:'',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'forget',component:ForgetpasswordComponent},
  {path:'resetpassword/:resetLink',component:ResetpasswordComponent},


  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
